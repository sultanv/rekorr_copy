﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace reKorr_Copy
{
  class Program
  {
    private static string _sourcePath;
    private static string _destPath;
    private static string programPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    private static bool TestMode = false;
    private static StreamWriter _logFile;
    private static bool _error = false;
    private static SqlDataReader _reader;
    static void Main(string[] args)
    {
      if (args.Count() > 0)
      {
        if (args[0].ToString().ToUpper() == "TEST")
        {
          TestMode = true;
        }
      }
      Console.WriteLine("reKorr - email fájlok másolása és rendezése.");
      Console.Write("Forrás könyvtár: ");
      _sourcePath = Console.ReadLine();
      Console.Write("Cél könyvtár: ");
      _destPath = Console.ReadLine();
      Console.WriteLine("A program a \"" + _sourcePath + "\" könyvtárból másolja az állományokat a \"" + _destPath + "\" könyvtárba.");
      Console.WriteLine("Indulhat a másolási folyamat? ([I]gen/[N]em)");
      string _key = Console.ReadLine();
      Console.WriteLine();
      if (_key.ToUpper().Trim() == "I")
      {
        if (System.IO.File.Exists(programPath + "\\reKorr.ini") == false)
        {
          Console.WriteLine("Nem található a reKorr.ini");
          return;
        }
        var parser = new FileIniDataParser();
        KeyData[] data = parser.ReadFile(programPath + "\\reKorr.ini")["SQL"].ToArray();
        string _ConnectionString = "";
        foreach (KeyData _item in data)
        {
          _ConnectionString += _item.KeyName + "=" + _item.Value + ";";
        }        
        using (SqlConnection conn = new SqlConnection(_ConnectionString))
        {
          try
          {
            if (conn.State != ConnectionState.Open)
              conn.Open();
            SqlCommand command = new SqlCommand(@"SELECT " + (TestMode == true ? " TOP 10 " : "") + " ker_evszam, ker_indito_levelezes, ker_erkezett_allasfoglalas FROM KereskedelmetErintoKorrekcioks", conn);
            _reader = command.ExecuteReader();
          }
          catch (Exception e)
          {
            Console.WriteLine("Hiba történt nem sikerült az adatbázis művelet. ");
            return;
          }
          //finally
          //{
          //  if (conn.State == ConnectionState.Open)
          //    conn.Close();
          //}
          int _cnt = 0;
          System.IO.File.Delete(programPath + "\\reKorr.log");
          _logFile = System.IO.File.CreateText(programPath + "\\reKorr.log");
          while (_reader.Read())
          {
            string path1 = _reader[1].ToString();
            string path2 = _reader[2].ToString();
            string year = _reader[0].ToString();
            try
            {
              if (CopyFile(Path.GetFileName(path1), year) == false) _cnt++;
              if (CopyFile(Path.GetFileName(path2), year) == false) _cnt++;
            } catch (Exception e)
            {
              _logFile.WriteLine("#Hiba " + path1 + ": " + e.Message);
              _error = true;
            }
          }
          _logFile.Close();
          
          if (_error) 
          { 
            Console.WriteLine("Másolás közben történtek hibák. reKorr.log létrehozva. ");
            return;
          }
        }
      } else
      {
        Console.WriteLine("Másolás megszakitva.");
      }
      Console.WriteLine("Nyomjon meg egy gombot a kilépéshez.");
      Console.ReadKey();
    }
    static bool CopyFile(string filename, string year)
    {
      bool _ret = true;
      // Use Path class to manipulate file and directory paths.
      string sourceFile = System.IO.Path.Combine(_sourcePath, filename);
      string targetPath = System.IO.Path.Combine(_destPath, year);
      string targetFile = System.IO.Path.Combine(targetPath, filename);
      // To copy a folder's contents to a new location:
      // Create a new target folder.
      // If the directory already exists, this method does not create a new directory.
      System.IO.Directory.CreateDirectory(targetPath);

      // To copy a file to another location and
      // overwrite the destination file if it already exists.
      if (System.IO.File.Exists(sourceFile))
      {
        System.IO.File.Copy(sourceFile, targetFile, true);
      }
      else
      {
        //Console.WriteLine("#A fájl nem található: " + sourceFile);
        _logFile.WriteLine("#A fájl nem található: " + sourceFile);
        _ret = false;
      }
      return _ret;
    }
  }
}
